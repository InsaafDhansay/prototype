import { FullCalendarModule, CalendarOptions } from '@fullcalendar/angular';
import { Component, Input, OnInit, NgModule } from '@angular/core';
import * as $ from 'jquery';
import * as moment from 'moment';
import 'fullcalendar';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-full-calendar',
  templateUrl: './full-calendar.component.html',
  styleUrls: ['./full-calendar.component.scss'],
})
export class FullCalendarComponent implements OnInit {
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth'
  };
  defaultConfigurations: any;
  @Input()
  set configurations(config: any) { //configurations overrides the default config object "defaultConfigurations"
    if (config) {
      this.defaultConfigurations = config;
    }
  }
  @Input() eventData: any; //array of all events to be displayed on the calender

  constructor() {
    this.defaultConfigurations = { //an object which is already assigned with basic values
      editable: true,
      eventLimit: true,
      titleFormat: 'MMM D YYYY',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay',
      },
      buttonText: {
        today: 'Today',
        month: 'Month',
        week: 'Week',
        day: 'Day',
      },
      views: {
        agenda: {
          eventLimit: 2,
        },
      },
      allDaySlot: false,
      slotDuration: moment.duration('00:15:00'),
      slotLabelInterval: moment.duration('01:00:00'),
      firstDay: 1,
      selectable: true,
      selectHelper: true,
      events: this.eventData, //passing events to full calender through the "event" property 
    };
    this.eventData = [
      {
         title: 'event1',
         start: moment()
      },
      {
         title: 'event2',
         start: moment(),
         end: moment().add(2, 'days')
      },
  ];
  }
  

  ngOnInit(): void {

  }
}




